#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <glog/logging.h>

#include <hpke/hpke.h>

#include <pdm/base_request.h>
#include <pdm/base_response.h>
#include <pdm/constants.h>
#include <pdm/utility.h>

#include <pdm/create_primary_server_shared_context_request.h>
#include <pdm/create_primary_server_shared_context_response.h>

#include <pdm/read_public_key_request.h>
#include <pdm/read_public_key_response.h>

// Given a IPv4 or IPv6 socket address,
// returns a pointer to appropriate sin_addr struct.
void* get_in_addr(struct sockaddr* sa);

int handle_create_shared_context_interactive(int argc, char* argv[], int sockfd);
int handle_read_public_key_interactive(int argc, char* argv[], int sockfd);

static int print_help()
{
  std::vector<std::string> lines = {
    "Usage: pdm_primary_client [OP_CODE] [COMMAND] [OPTIONS]",
    "",
    "Available Subcommands:",
    "",
    "* create shared_context [ip_address] [--suite_id=<suite_id>]",
    "",
    "Create a shared context with a primary server.",
    "",
    "Suite id is KEM ID, KDF ID and AEAD ID seperated by commas without any space.",
    "Serialized public key will be read from configuration.",
    "",
    "> pdm_primary_client create shared_context 127.0.0.1 --suite_id=32,1,1",
    "",
    "* read public_key [ip_address] [--suite_id=<suite_id>]",
    "",
    "Read the public key of suite preferred by primary server.",
    "",
    "> pdm_primary_client read public_key 127.0.0.1 --suite_id=32,1,1, --suite_id=32,1,2",
    "",
    "--help: Print help instructions"
  };

  for (const auto& line : lines)
    std::cout << line << std::endl;

  return 0;
}

int main(int argc, char* argv[])
{
  if (argc == 1 || std::string(argv[1]) == "--help")
    return print_help();

  FLAGS_logtostderr = true;
  google::InitGoogleLogging(argv[0]);

  int sockfd;
  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(argv[3], PDM::DEFAULT_PRIMARY_SERVER_PORT, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  for (p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      PLOG(WARNING);
      continue;
    }

    if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      PLOG(WARNING);
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return 2;
  }

  inet_ntop(p->ai_family, get_in_addr((struct sockaddr*)p->ai_addr), s, sizeof s);

  freeaddrinfo(servinfo);

  LOG(INFO) << "Connecting to " << s << std::endl;

  std::string op_code(argv[1]), command(argv[2]);

  if (op_code == "create" && command == "shared_context")
    return handle_create_shared_context_interactive(argc, argv, sockfd);

  if (op_code == "read" && command == "public_key")
    return handle_read_public_key_interactive(argc, argv, sockfd);

  // TODO: Handle unrecognized commands

  return 0;
}

// pdm_primary_client create shared_context 127.0.0.1 --suite_id=32,1,1
int handle_create_shared_context_interactive(int argc, char* argv[], int sockfd)
{
  if (argc != 5) {
    std::cout << "Failure: must have exactly four arguments. Check usage with 'pdm_primary_client --help'" << std::endl;
    return 1;
  }

  unsigned char recvbuf[PDM::MAXIMUM_BUFFER_SIZE];

  auto client_id = std::string(argv[3]);

  // Strip '--suite_id='
  auto suite_id = std::string(argv[4]).substr(11);

  auto suite = PDM::build_hpke_suite(suite_id);

  // Assume that primary client has negotiated the suite and has primary server's public key.
  auto public_key = PDM::read_public_key(PDM::DEFAULT_PRIMARY_CLIENT_CONFIG_BASE_DIR, suite, client_id);
  auto sender_info = suite.setup_base_s(*public_key, {});

  // Pass encapsulated secret
  PDM::CreatePrimaryServerSharedContextRequest request(suite, sender_info.first);
  const auto request_frame = request.to_bytes();

  LOG(INFO) << request << std::endl;

  if (send(sockfd, request_frame.data(), request_frame.size(), 0) == -1)
    PLOG(WARNING);

  if ((recv(sockfd, recvbuf, PDM::MAXIMUM_BUFFER_SIZE - 1, 0)) == -1)
    PLOG(WARNING);

  auto frame_size = PDM::Header::extract_frame_size(recvbuf);
  std::vector<uint8_t> response_frame(recvbuf, recvbuf + frame_size);
  PDM::CreatePrimaryServerSharedContextResponse response(response_frame);

  if (response._status)
    LOG(ERROR) << response << std::endl;
  else {
    LOG(INFO) << response << std::endl;
    PDM::write_shared_context(PDM::DEFAULT_PRIMARY_CLIENT_CONFIG_BASE_DIR, sender_info.second, client_id);
  }

  return response._status;
}

int handle_read_public_key_interactive(int argc, char* argv[], int sockfd)
{
  if (argc < 5) {
    std::cout << "Failure: must have at least four arguments. Check usage with 'pdm_primary_client --help'" << std::endl;
    return 1;
  }

  auto client_id = std::string(argv[3]);
  unsigned char recvbuf[PDM::MAXIMUM_BUFFER_SIZE];

  std::vector<PDM::HPKE> suites;
  for (int i = 4; i < argc; i++)
    suites.push_back(PDM::build_hpke_suite(std::string(argv[i]).substr(11)));

  PDM::ReadPublicKeyRequest request(suites);

  LOG(INFO) << request << std::endl;
  const auto request_frame = request.to_bytes();

  if (send(sockfd, request_frame.data(), request_frame.size(), 0) == -1)
    PLOG(WARNING);

  if ((recv(sockfd, recvbuf, PDM::MAXIMUM_BUFFER_SIZE - 1, 0)) == -1)
    PLOG(WARNING);

  auto frame_size = PDM::Header::extract_frame_size(recvbuf);

  bytes response_frame(recvbuf, recvbuf + frame_size);
  LOG(INFO) << response_frame << std::endl;

  PDM::ReadPublicKeyResponse response(response_frame);

  if (response._status)
    LOG(ERROR) << response << std::endl;
  else {
    LOG(INFO) << response << std::endl;
    PDM::write_serialized_public_key(PDM::DEFAULT_PRIMARY_CLIENT_CONFIG_BASE_DIR, response._suite.value(), client_id, to_hex(response._serialized_public_key));
  }

  return response._status;
}

void* get_in_addr(struct sockaddr* sa)
{
  if (sa->sa_family == AF_INET)
    return &(((struct sockaddr_in*)sa)->sin_addr);

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
