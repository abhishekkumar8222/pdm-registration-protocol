#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <glog/logging.h>

#include <hpke/hpke.h>

#include <pdm/constants.h>
#include <pdm/utility.h>

// TODO: Remove in future as we start to use libhpke.
static hpke::HPKE suite(
    hpke::KEM::ID::DHKEM_X25519_SHA256,
    hpke::KDF::ID::HKDF_SHA256,
    hpke::AEAD::ID::AES_128_GCM);

// Given a IPv4 or IPv6 socket address,
// returns pointer to appropriate sin_addr structure.
void* get_in_addr(struct sockaddr* sa);

int main(int argc, char* argv[])
{
  // By default, log to standard error.
  FLAGS_logtostderr = true;
  google::InitGoogleLogging(argv[0]);

  int sockfd, new_fd;
  unsigned char buf[PDM::MAXIMUM_BUFFER_SIZE];
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_storage their_addr;
  socklen_t sin_size;

  int yes = 1;
  char s[INET6_ADDRSTRLEN];
  int rv;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if ((rv = getaddrinfo(NULL, PDM::DEFAULT_PRIMARY_SERVER_PORT, &hints, &servinfo)) != 0) {
    LOG(FATAL) << "Failed to get address information: " << gai_strerror(rv) << std::endl;
    return 1;
  }

  for (p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      PLOG(WARNING);
      continue;
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
      PLOG(FATAL);
      return 1;
    }

    if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      PLOG(WARNING);
      continue;
    }

    break;
  }

  freeaddrinfo(servinfo);

  if (p == NULL) {
    LOG(FATAL) << "Failed to bind!" << std::endl;
    return 1;
  }

  if (listen(sockfd, PDM::DEFAULT_PRIMARY_SERVER_BACKLOG) == -1) {
    PLOG(FATAL);
    return 1;
  }

  LOG(INFO) << "Listening on port: " << PDM::DEFAULT_PRIMARY_SERVER_PORT << std::endl;
  LOG(INFO) << "Allowed BACKLOG: " << PDM::DEFAULT_PRIMARY_SERVER_BACKLOG << std::endl;

  while (1) {
    sin_size = sizeof their_addr;
    new_fd = accept(sockfd, (struct sockaddr*)&their_addr, &sin_size);

    if (new_fd == -1) {
      PLOG(WARNING);
      continue;
    }

    inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr), s, sizeof s);

    LOG(INFO) << "Got connection from " << s << std::endl;

    if ((recv(new_fd, buf, PDM::MAXIMUM_BUFFER_SIZE - 1, 0)) == -1) {
      PLOG(FATAL);
      exit(1);
    }

    auto response_frame = PDM::build_response_frame(buf);

    if (!response_frame.empty()) {
      if (send(new_fd, response_frame.data(), response_frame.size(), 0) == -1)
        PLOG(WARNING);
    }

    close(new_fd);

    LOG(INFO) << "Closed connection for " << s << std::endl;
  }

  return 0;
}

void* get_in_addr(struct sockaddr* sa)
{
  if (sa->sa_family == AF_INET)
    return &((struct sockaddr_in*)sa)->sin_addr;

  return &((struct sockaddr_in6*)sa)->sin6_addr;
}
