#include <pdm/base_response.h>

PDM::BaseResponse::BaseResponse(const PDM::BaseRequest& request)
    : _header(request._header)
{
  _header._request = 2;
}

PDM::BaseResponse::BaseResponse(const std::vector<uint8_t>& bytes)
    : _header(bytes) {};

std::ostream&
operator<<(std::ostream& os, const PDM::BaseResponse& response)
{
  os << "PDM::BaseResponse("
     << "header=" << response._header << ")";

  return os;
}
