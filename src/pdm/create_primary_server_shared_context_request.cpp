#include <pdm/create_primary_server_shared_context_request.h>

PDM::CreatePrimaryServerSharedContextRequest::CreatePrimaryServerSharedContextRequest(
    const hpke::HPKE& suite,
    const bytes& encapsulated_secret)
    : _suite(suite)
    , _encapsulated_secret(encapsulated_secret)
{
  _header._length = PDM::Header::size()
      + 3 * 2 // 2 bytes each for KEM, KDF and AEAD ID
      + _encapsulated_secret.size();

  _header._record_type = 2;     // Application Data
  _header._sender_type = 1;     // Primary Client
  _header._authority_level = 1; // Normal User
  _header._command = 1;         // Primary Client-Primary Server Shared Context
  _header._op_code = 1;         // Create
}

PDM::CreatePrimaryServerSharedContextRequest::CreatePrimaryServerSharedContextRequest(
    const std::vector<uint8_t>& bytes)
    : BaseRequest(bytes)
    , _suite(
          static_cast<hpke::KEM::ID>((bytes[16] << 8) | bytes[17]),
          static_cast<hpke::KDF::ID>((bytes[18] << 8) | bytes[19]),
          static_cast<hpke::AEAD::ID>((bytes[20] << 8) | bytes[21]))
    , _encapsulated_secret(bytes.begin() + 22, bytes.end()) {};

std::vector<uint8_t> PDM::CreatePrimaryServerSharedContextRequest::to_bytes() const
{
  auto header_bytes = _header.to_bytes();

  header_bytes.emplace_back(static_cast<unsigned int>(_suite.kem.id) >> 8);
  header_bytes.emplace_back(static_cast<unsigned int>(_suite.kem.id));
  header_bytes.emplace_back(static_cast<unsigned int>(_suite.kdf.id) >> 8);
  header_bytes.emplace_back(static_cast<unsigned int>(_suite.kdf.id));
  header_bytes.emplace_back(static_cast<unsigned int>(_suite.aead.id) >> 8);
  header_bytes.emplace_back(static_cast<unsigned int>(_suite.aead.id));

  header_bytes.insert(
      header_bytes.end(),
      _encapsulated_secret.begin(),
      _encapsulated_secret.end());

  return header_bytes;
}

std::ostream& operator<<(std::ostream& os, const PDM::CreatePrimaryServerSharedContextRequest& request)
{
  // TODO: Display suite too
  os << "PDM::CreatePrimaryServerSharedContextRequest("
     << "header=" << request._header << ", "
     << "encapsulated_secret=" << to_hex(request._encapsulated_secret) << ")";

  return os;
}
