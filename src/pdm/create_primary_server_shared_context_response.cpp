#include <hpke/hpke.h>

#include <pdm/constants.h>
#include <pdm/create_primary_server_shared_context_response.h>
#include <pdm/utility.h>

PDM::CreatePrimaryServerSharedContextResponse::CreatePrimaryServerSharedContextResponse(
    const PDM::CreatePrimaryServerSharedContextRequest& request)
    : BaseResponse(request)
{
  try {
    // TODO: Check if shared context already exists and skip the below steps.

    // Optimization: deserialize all private keys during process start-up and store in a map
    // TODO: Replace "127.0.0.1" with primary server's client id
    auto private_key = PDM::read_private_key(DEFAULT_PRIMARY_SERVER_CONFIG_BASE_DIR, request._suite, "127.0.0.1");

    auto receiver_context = request._suite.setup_base_r(request._encapsulated_secret, *private_key, {});

    _status = 0; // OK

    // TODO: Replace "127.0.0.1" with primary client's client id
    write_shared_context(DEFAULT_PRIMARY_SERVER_CONFIG_BASE_DIR, receiver_context, "127.0.0.1");
  } catch (const std::exception& e) {
    _status = 1; // Unprocessable Entity
    _message = e.what();
  }

  _header._length = PDM::Header::size()
      + 1                // Status code
      + _message.size(); // Error message if any
}

PDM::CreatePrimaryServerSharedContextResponse::CreatePrimaryServerSharedContextResponse(
    const std::vector<uint8_t>& bytes)
    : BaseResponse(bytes)
    , _status(bytes[16])
    , _message(bytes.begin() + 17, bytes.end()) {};

std::vector<uint8_t> PDM::CreatePrimaryServerSharedContextResponse::to_bytes() const
{
  auto header_bytes = _header.to_bytes();

  header_bytes.emplace_back(_status);
  header_bytes.insert(header_bytes.end(), _message.begin(), _message.end());

  return header_bytes;
}

std::ostream& operator<<(std::ostream& os, const PDM::CreatePrimaryServerSharedContextResponse& response)
{
  os << "PDM::CreatePrimaryServerSharedContextResponse("
     << "header=" << response._header << ", "
     << "status=" << static_cast<unsigned int>(response._status);

  if (response._status)
    os << ", message='" << response._message << "'";

  os << ")";

  return os;
}
