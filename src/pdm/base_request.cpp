#include <iostream>
#include <pdm/base_request.h>

PDM::BaseRequest::BaseRequest()
{
  _header._request = 1;
};

PDM::BaseRequest::BaseRequest(const std::vector<uint8_t>& bytes)
    : _header(bytes) {};

void PDM::BaseRequest::prepare()
{
  _header._sequence_number = _header._transaction_id = generate_sequence_number();
}

uint16_t PDM::BaseRequest::generate_sequence_number()
{
  return 1;
}

std::ostream& operator<<(std::ostream& os, const PDM::BaseRequest& request)
{
  os << "PDM::BaseRequest("
     << "header=" << request._header << ")";

  return os;
}
