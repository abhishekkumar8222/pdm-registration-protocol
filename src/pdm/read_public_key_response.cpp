#include <pdm/constants.h>
#include <pdm/read_public_key_response.h>
#include <pdm/utility.h>

PDM::ReadPublicKeyResponse::ReadPublicKeyResponse(
    const PDM::ReadPublicKeyRequest& request)
    : BaseResponse(request)
{
  try {
    _status = 0;

    _header._length = PDM::Header::size() + 2; // Status code

    // TODO: Read preferred suites from a file
    _suite.emplace(request._suites[0]);

    // TODO: Replace 127.0.0.1 with primary server's client id.
    _serialized_public_key = read_serialized_public_key(DEFAULT_PRIMARY_SERVER_CONFIG_BASE_DIR, _suite.value(), "127.0.0.1");

    _header._length += (PDM::HPKE::size() + 2 + _serialized_public_key.size());
  } catch (const std::exception& e) {
    _status = 1; // Unprocessable Entity
    _message = e.what();
    _header._length += (2 + _message.size());
  }
}

PDM::ReadPublicKeyResponse::ReadPublicKeyResponse(
    const bytes& frame)
    : BaseResponse(frame)
{
  size_t offset = 16;

  _status = PDM::extract<uint16_t>(frame, offset);

  if (_status) {
    _message = PDM::extract<std::string>(frame, offset);
  } else {
    _suite.emplace(PDM::extract<PDM::HPKE>(frame, offset));
    _serialized_public_key = PDM::extract<bytes>(frame, offset);
  }
}

std::ostream& operator<<(std::ostream& os, const PDM::ReadPublicKeyResponse& response)
{
  os << "PDM::ReadPublicKeyResponse(header=" << response._header << ", status=" << static_cast<unsigned int>(response._status);

  if (response._status)
    os << ", message='" << response._message << "'";
  else {
    os << ", suite=" << response._suite.value() << ", serialized_public_key='" << to_hex(response._serialized_public_key) << "'";
  }

  os << ")";

  return os;
}

bytes PDM::ReadPublicKeyResponse::to_bytes() const
{
  auto header_bytes = _header.to_bytes();

  header_bytes.emplace_back(_status >> 8);
  header_bytes.emplace_back(_status);

  if (_status) {
    header_bytes.emplace_back(_message.size() >> 8);
    header_bytes.emplace_back(_message.size());

    header_bytes.insert(header_bytes.end(), _message.begin(), _message.end());
  } else {
    auto suite_bytes = _suite.value().to_bytes();
    header_bytes.insert(header_bytes.end(), suite_bytes.begin(), suite_bytes.end());

    header_bytes.emplace_back(_serialized_public_key.size() >> 8);
    header_bytes.emplace_back(_serialized_public_key.size());

    header_bytes.insert(header_bytes.end(), _serialized_public_key.begin(), _serialized_public_key.end());
  }

  return header_bytes;
}
