#include <pdm/read_public_key_request.h>
#include <pdm/utility.h>

PDM::ReadPublicKeyRequest::ReadPublicKeyRequest(
    const std::vector<PDM::HPKE>& suites)
    : _suites(suites)
{
  _header._length = PDM::Header::size()
      + 2                       // 2 bytes for the count of suites passed
      + 3 * 2 * _suites.size(); // 2 bytes each for KEM, KDF and AEAD for each suite.

  _header._record_type = 2;     // Application Data
  _header._sender_type = 1;     // Primary Client
  _header._authority_level = 1; // Normal User
  _header._command = 2;         // Public Key
  _header._op_code = 4;         // Read
}

PDM::ReadPublicKeyRequest::ReadPublicKeyRequest(const bytes& frame)
    : BaseRequest(frame)
{
  auto base_itr = frame.begin() + PDM::Header::size() + 2;

  for (auto itr = base_itr; itr != frame.end(); itr += PDM::HPKE::size()) {
    bytes suite_bytes(itr, itr + PDM::HPKE::size());

    _suites.push_back(PDM::HPKE(suite_bytes));
  }
}

bytes PDM::ReadPublicKeyRequest::to_bytes() const
{
  auto header_bytes = _header.to_bytes();

  header_bytes.emplace_back(static_cast<unsigned int>(_suites.size()) >> 8);
  header_bytes.emplace_back(static_cast<unsigned int>(_suites.size()));

  for (const auto& suite : _suites) {
    auto suite_bytes = suite.to_bytes();

    header_bytes.insert(header_bytes.end(), suite_bytes.begin(), suite_bytes.end());
  }

  return header_bytes;
}

std::ostream& operator<<(std::ostream& os, const PDM::ReadPublicKeyRequest& request)
{
  os << "PDM::ReadPublicKeyRequest(header=" << request._header << ", suites=" << request._suites << ")";

  return os;
}
