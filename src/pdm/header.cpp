#include <iostream>
#include <pdm/header.h>

PDM::Header::Header()
    : _pdm_version(2)
    , _hpke_version(0)
    , _record_type(2) {};

PDM::Header::Header(const std::vector<uint8_t>& bytes)
    : _pdm_version(bytes[0])
    , _length((bytes[1] << 8) | bytes[2])
    , _hpke_version(bytes[3])
    , _record_type(bytes[4])
    , _sequence_number((bytes[5] << 8) | bytes[6])
    , _sender_type(bytes[7])
    , _authority_level(bytes[8])
    , _command((bytes[9] << 8) | bytes[10])
    , _request(bytes[11])
    , _transaction_id((bytes[12] << 8) | bytes[13])
    , _op_code(bytes[14]) {};

std::vector<uint8_t> PDM::Header::to_bytes() const
{
  return std::vector<uint8_t> {
    _pdm_version,
    (uint8_t)(_length >> 8),
    (uint8_t)(_length),
    _hpke_version,
    _record_type,
    (uint8_t)(_sequence_number >> 8),
    (uint8_t)(_sequence_number),
    _sender_type,
    _authority_level,
    (uint8_t)(_command >> 8),
    (uint8_t)(_command),
    _request,
    (uint8_t)(_transaction_id >> 8),
    (uint8_t)(_transaction_id),
    _op_code,
    0 // Eight reserved bits
  };
}

std::ostream& operator<<(std::ostream& os, const PDM::Header& header)
{
  os << "PDM::Header("
     << "pdm_version=" << static_cast<unsigned int>(header._pdm_version) << ", "
     << "length=" << static_cast<unsigned int>(header._length) << ", "
     << "hpke_version=" << static_cast<unsigned int>(header._hpke_version) << ", "
     << "record_type=" << static_cast<unsigned int>(header._record_type) << ", "
     << "sequence_number=" << static_cast<unsigned int>(header._sequence_number) << ", "
     << "sender_type=" << static_cast<unsigned int>(header._sender_type) << ", "
     << "authority_level=" << static_cast<unsigned int>(header._authority_level) << ", "
     << "command=" << static_cast<unsigned int>(header._command) << ", "
     << "request=" << static_cast<unsigned int>(header._request) << ", "
     << "transaction_id=" << static_cast<unsigned int>(header._transaction_id) << ", "
     << "op_code=" << static_cast<unsigned int>(header._op_code) << ")";

  return os;
}

uint8_t PDM::Header::extract_op_code(const unsigned char buffer[]) { return buffer[14]; }

size_t PDM::Header::extract_frame_size(const unsigned char buffer[])
{
  return (static_cast<unsigned int>(buffer[1]) << 8) | static_cast<unsigned int>(buffer[2]);
}

uint16_t PDM::Header::extract_command(const unsigned char buffer[])
{
  return (static_cast<unsigned int>(buffer[9]) << 8) | static_cast<unsigned int>(buffer[10]);
}
