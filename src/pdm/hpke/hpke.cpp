#include <ostream>

#include <pdm/hpke/hpke.h>

PDM::HPKE::HPKE(const unsigned int& kem_id, const unsigned int& kdf_id, const unsigned int& aead_id)
    : HPKE::HPKE(
        static_cast<hpke::KEM::ID>(kem_id),
        static_cast<hpke::KDF::ID>(kdf_id),
        static_cast<hpke::AEAD::ID>(aead_id)) {};

PDM::HPKE::HPKE(const bytes& bytes)
    : HPKE::HPKE(
        static_cast<hpke::KEM::ID>((bytes[0] << 8) | bytes[1]),
        static_cast<hpke::KDF::ID>((bytes[2] << 8) | bytes[3]),
        static_cast<hpke::AEAD::ID>((bytes[4] << 8) | bytes[5])) {};

std::ostream& operator<<(std::ostream& os, const PDM::HPKE& suite)
{
  os << "PDM::HPKE(kem_id=" << static_cast<unsigned int>(suite.kem.id)
     << ", kdf_id=" << static_cast<unsigned int>(suite.kdf.id)
     << ", aead_id=" << static_cast<unsigned int>(suite.aead.id) << ")";

  return os;
}

bytes PDM::HPKE::to_bytes() const
{
  return bytes {
    (static_cast<uint16_t>(kem.id) >> 8),
    static_cast<uint16_t>(kem.id),
    (static_cast<uint16_t>(kdf.id) >> 8),
    static_cast<uint16_t>(kdf.id),
    (static_cast<uint16_t>(aead.id) >> 8),
    static_cast<uint16_t>(aead.id)
  };
}
