#include <fstream>
#include <string>

#include <glog/logging.h>
#include <hpke/hpke.h>

#include <pdm/header.h>
#include <pdm/utility.h>

#include <pdm/create_primary_server_shared_context_request.h>
#include <pdm/create_primary_server_shared_context_response.h>

#include <pdm/read_public_key_request.h>
#include <pdm/read_public_key_response.h>

bytes PDM::build_response_frame(const unsigned char buffer[])
{
  auto command = PDM::Header::extract_command(buffer);
  auto op_code = PDM::Header::extract_op_code(buffer);
  auto frame_size = PDM::Header::extract_frame_size(buffer);

  bytes request_frame(buffer, buffer + frame_size);

  if (command == 1 && op_code == 1) {
    PDM::CreatePrimaryServerSharedContextRequest request(request_frame);
    LOG(INFO) << request << std::endl;

    PDM::CreatePrimaryServerSharedContextResponse response(request);
    LOG(INFO) << response << std::endl;

    return response.to_bytes();
  } else if (command == 2 && op_code == 4) {
    PDM::ReadPublicKeyRequest request(request_frame);
    LOG(INFO) << request << std::endl;

    PDM::ReadPublicKeyResponse response(request);
    LOG(INFO) << response << std::endl;

    return response.to_bytes();
  } else
    // Unrecognised Command-Opcode
    return std::vector<uint8_t> {};
}

std::string to_string(const hpke::KEM& kem) { return std::to_string(static_cast<unsigned int>(kem.id)); }
std::string to_string(const hpke::KDF& kdf) { return std::to_string(static_cast<unsigned int>(kdf.id)); }
std::string to_string(const hpke::AEAD& aead) { return std::to_string(static_cast<unsigned int>(aead.id)); }

std::string to_string(const hpke::HPKE& suite)
{
  return to_string(suite.kem) + "_" + to_string(suite.kdf) + "_" + to_string(suite.aead);
}

/*
 * This can be ideally be `suite.read_serialized_private_key(PRIMARY_CLIENT)` but does not fit with libhpke's code.
 * If I ever write a wrapper over hpke::HPKE, move this too.
 */
std::unique_ptr<hpke::KEM::PrivateKey> PDM::read_private_key(const std::filesystem::path& base_dir, const hpke::HPKE& suite, const std::string& client_id)
{
  std::string line;
  std::ifstream serialized_private_key_file(base_dir / "keys" / client_id / to_string(suite) / "private_key");

  if (!serialized_private_key_file)
    throw std::runtime_error("Unable to open private key file");

  if (!std::getline(serialized_private_key_file, line))
    throw std::runtime_error("Private key file is empty");

  return suite.kem.deserialize_private(from_hex(line));
}

bytes PDM::read_serialized_public_key(const std::filesystem::path& base_dir, const hpke::HPKE& suite, const std::string& client_id)
{
  std::string line;
  std::ifstream serialized_public_key_file(base_dir / "keys" / client_id / to_string(suite) / "public_key");

  if (!serialized_public_key_file)
    throw std::runtime_error("Unable to open public key file");

  if (!std::getline(serialized_public_key_file, line))
    throw std::runtime_error("Public key file is empty");

  return from_hex(line);
}

std::unique_ptr<hpke::KEM::PublicKey> PDM::read_public_key(const std::filesystem::path& base_dir, const hpke::HPKE& suite, const std::string& client_id)
{
  return suite.kem.deserialize(read_serialized_public_key(base_dir, suite, client_id));
}

void PDM::write_serialized_public_key(const std::filesystem::path& base_dir, const hpke::HPKE& suite, const std::string& client_id, const std::string& serialized_public_key)
{
  std::filesystem::path serialized_public_key_dir(base_dir / "keys" / client_id / to_string(suite));
  std::filesystem::path serialized_public_key_path(serialized_public_key_dir / "public_key");

  std::filesystem::create_directories(serialized_public_key_dir);

  std::ofstream serialized_public_key_file(serialized_public_key_path);

  if (!serialized_public_key_file)
    throw std::runtime_error("Unable to open serialized public key file");

  if (!(serialized_public_key_file << serialized_public_key))
    throw std::runtime_error("Unable to write serialized public key");

  LOG(INFO) << "Wrote serialized public key for " << client_id << " to: " << serialized_public_key_path << std::endl;
}

void PDM::write_shared_context(const std::filesystem::path& base_dir, const hpke::Context& context, const std::string& client_id)
{
  std::filesystem::path shared_context_file_dir(base_dir / "shared_contexts");
  std::filesystem::path shared_context_file_path = base_dir / "shared_contexts" / client_id;

  std::filesystem::create_directories(shared_context_file_dir);

  std::ofstream shared_context_file(shared_context_file_path);

  if (!shared_context_file)
    throw std::runtime_error("Unable to open shared context file");

  if (!(shared_context_file << context))
    throw std::runtime_error("Unable to write shared context");

  LOG(INFO) << "Wrote shared context for " << client_id << " to: " << shared_context_file_path << std::endl;
}

PDM::HPKE PDM::build_hpke_suite(const std::string& suite_id)
{
  auto first_comma = suite_id.find(",");
  auto second_comma = suite_id.find(",", first_comma + 1);

  unsigned int kem_id = stoi(suite_id.substr(0, first_comma)),
               kdf_id = stoi(suite_id.substr(first_comma + 1, (second_comma - (first_comma + 1)))),
               aead_id = stoi(suite_id.substr(second_comma + 1));

  return PDM::HPKE(kem_id, kdf_id, aead_id);
}

/*
 * As uint8_t is a typedef of unsigned char, cast it to unsigned int before
 * before printing.
 */
template <>
std::ostream& operator<<(std::ostream& os, const std::vector<uint8_t>& v)
{
  os << "[" << std::hex;

  for (int i = 0; i < v.size(); i++) {
    if (i)
      os << " ";

    os << static_cast<unsigned int>(v[i]);
  }

  os << "]" << std::dec;

  return os;
}

template <>
uint16_t PDM::extract(const bytes& frame, size_t& offset)
{
  auto value = (static_cast<uint16_t>(frame[offset]) << 8) | static_cast<uint16_t>(frame[offset + 1]);
  offset += 2;

  return value;
}

template <>
bytes PDM::extract(const bytes& frame, size_t& offset)
{
  auto length = PDM::extract<uint16_t>(frame, offset);
  bytes value(frame.begin() + offset, frame.begin() + (offset + length));
  offset += length;

  return value;
}

template <>
std::string PDM::extract(const bytes& frame, size_t& offset)
{
  auto length = PDM::extract<uint16_t>(frame, offset);
  std::string value(frame.begin() + offset, frame.begin() + (offset + length));
  offset += length;

  return value;
}

template <>
PDM::HPKE PDM::extract(const bytes& frame, size_t& offset)
{
  bytes suite_bytes(frame.begin() + offset, frame.begin() + offset + PDM::HPKE::size());

  PDM::HPKE value(suite_bytes);
  offset += PDM::HPKE::size();

  return value;
}
