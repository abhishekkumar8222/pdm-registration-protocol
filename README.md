# PDMv2 Registration Server

## Installation

- The project uses Git Submodules, so clone using:

```
git clone https://gitlab.com/abhishekkumar8222/pdm-registration-protocol.git --recurse-submodules
```

If you have already cloned the repo, run `git submodule init` to initialize your local configuration file and then `git submodule update` to fetch data and check out the appropriate commit.

- Install CMake and OpenSSL

- `make build` - this will create two executables `pdm_primary_client`
  and `pdm_primary_server` in the `build` directory.

- If you want the executables to be in your bin directories, use `make install`.

## Dissector Usage

The directory `dissectors/` contains a Wireshark Lua dissector for PDMv2 Registration Protocol.

Use the following command to load the Lua dissctor for analzying the captured packets:
```
sudo wireshark -X lua_script:Documents/Lua/packet_pdm_registration.lua
```

The directory `dissectors/examples` contains PCAP capture file(s).

## TODO

- [ ] Break down the project into smaller parts
- [ ] Read server port number, backlog from a configuration file
