BUILD_DIR=build

.PHONY: all generate-${BUILD_DIR} ${BUILD_DIR}

all: build

# Generate project buildsystem using CMAKE
generate-${BUILD_DIR}:
	cmake -B${BUILD_DIR} .

# Build the project libraries and executables
${BUILD_DIR}: generate-${BUILD_DIR}
	cmake --build ${BUILD_DIR}

clean:
	rm -rf ${BUILD_DIR}
