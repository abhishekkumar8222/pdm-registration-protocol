#ifndef PDM_CREATE_PRIMARY_SERVER_SHARED_CONTEXT_RESPONSE_H
#define PDM_CREATE_PRIMARY_SERVER_SHARED_CONTEXT_RESPONSE_H

#include <pdm/base_response.h>
#include <pdm/create_primary_server_shared_context_request.h>

namespace PDM {
  class CreatePrimaryServerSharedContextResponse : public BaseResponse {
    public:
      CreatePrimaryServerSharedContextResponse(const PDM::CreatePrimaryServerSharedContextRequest &request);

      CreatePrimaryServerSharedContextResponse(const std::vector<uint8_t> &bytes);

      std::vector<uint8_t> to_bytes() const;

      uint8_t _status;
      std::string _message;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::CreatePrimaryServerSharedContextResponse &response);

#endif
