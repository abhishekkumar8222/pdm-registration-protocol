#ifndef PDM_UTILITY_H
#define PDM_UTILITY_H

#include<iomanip>
#include<filesystem>

#include <pdm/hpke/hpke.h>

/*
 * A nicer way to print std::vector
 */
template <class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
  os << "[";

  for (size_t i = 0; i < v.size() - 1; i++)
    os << v[i] << ", ";
  os << v.back();

  os << "]";

  return os;
}

namespace PDM {
  bytes build_response_frame(const unsigned char buffer[]);

  std::unique_ptr<hpke::KEM::PrivateKey> read_private_key(const std::filesystem::path &base_dir, const hpke::HPKE &suite, const std::string &client_id);

  bytes read_serialized_public_key(const std::filesystem::path &base_dir, const hpke::HPKE &suite, const std::string &client_id);

  std::unique_ptr<hpke::KEM::PublicKey> read_public_key(const std::filesystem::path &base_dir, const hpke::HPKE &suite, const std::string &client_id);

  void write_serialized_public_key(const std::filesystem::path &base_dir, const hpke::HPKE &suite, const std::string &client_id, const std::string &serialized_public_key);
  void write_shared_context(const std::filesystem::path &base_dir, const hpke::Context &context, const std::string &client_id);

  PDM::HPKE build_hpke_suite(const std::string &suite_id);

  template <class T>
  T extract(const bytes &frame, size_t &offset);
}

#endif
