#ifndef PDM_HPKE_HPKE_H
#define PDM_HPKE_HPKE_H

/* A drop-in replacement for hpke::HPKE, implements PDM-specific helper functions */

#include <hpke/hpke.h>

namespace PDM {
  struct HPKE: public hpke::HPKE {
    HPKE(const hpke::HPKE &suite) : hpke::HPKE(suite) {};

    HPKE(const hpke::KEM::ID &kem_id, const hpke::KDF::ID &kdf_id, const hpke::AEAD::ID &aead_id) : hpke::HPKE(kem_id, kdf_id, aead_id) {};

    HPKE(const unsigned int &kem_id, const unsigned int &kdf_id, const unsigned int &aead_id);

    HPKE(const bytes  &bytes);

    static size_t size() { return 6; } // 6 bytes

    bytes to_bytes() const;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::HPKE &suite);

#endif
