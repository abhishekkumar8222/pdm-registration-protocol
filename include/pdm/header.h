#ifndef PDM_HEADER_H
#define PDM_HEADER_H

#include <cstdint>
#include <vector>
#include <ostream>

namespace PDM {
  class Header {
    public:
    Header();

    Header(const std::vector<uint8_t> &bytes);

    std::vector<uint8_t> to_bytes() const;

    static unsigned int size() { return 16; } // 16 bytes

    static uint8_t extract_op_code(const unsigned char buffer[]);
    static size_t extract_frame_size(const unsigned char buffer[]);
    static uint16_t extract_command(const unsigned char buffer[]);

    /*
     * TODO: Move pdm_version, hpke_version, record type, sender type,
     * authority_level, command, request, op_code to enums.
     */
    uint8_t  _pdm_version;
    uint16_t _length;
    uint8_t  _hpke_version;
    uint8_t  _record_type;
    uint16_t _sequence_number;
    uint8_t  _sender_type;
    uint8_t  _authority_level;
    uint16_t _command;
    uint8_t  _request;
    uint16_t _transaction_id;
    uint8_t  _op_code;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::Header &header);

#endif
