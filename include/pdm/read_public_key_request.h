#ifndef PDM_READ_PUBLIC_KEY_REQUEST_H
#define PDM_READ_PUBLIC_KEY_REQUEST_H

#include <pdm/hpke/hpke.h>
#include <pdm/base_request.h>

namespace PDM {
  class ReadPublicKeyRequest : public PDM::BaseRequest {
    public:
      ReadPublicKeyRequest(const std::vector<PDM::HPKE> &suites);

      ReadPublicKeyRequest(const bytes &frame);

      bytes to_bytes() const;

      std::vector<PDM::HPKE> _suites;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::ReadPublicKeyRequest &request);

#endif
