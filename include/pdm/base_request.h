#ifndef PDM_BASE_REQUEST_H
#define PDM_BASE_REQUEST_H

#include <ostream>
#include <pdm/header.h>

namespace PDM {
  class BaseRequest {
  public: 
    BaseRequest();

    BaseRequest(const std::vector<uint8_t> &bytes);

    /*
     * Prepare request - assigning packet length, sequence number
     * and transaction id.
     */
    void prepare();

    static uint16_t generate_sequence_number();

    std::vector<uint8_t> to_bytes() const { return _header.to_bytes(); }

    PDM::Header _header;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::BaseRequest &request);

#endif
