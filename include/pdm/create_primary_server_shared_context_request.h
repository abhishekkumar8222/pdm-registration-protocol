#ifndef PDM_CREATE_PRIMARY_SERVER_SHARED_CONTEXT_REQUEST_H
#define PDM_CREATE_PRIMARY_SERVER_SHARED_CONTEXT_REQUEST_H

#include <hpke/hpke.h>
#include <pdm/base_request.h>

namespace PDM {
  class CreatePrimaryServerSharedContextRequest : public PDM::BaseRequest {
    public:
      CreatePrimaryServerSharedContextRequest(const hpke::HPKE &suite, const bytes &encapsulated_secret);

      CreatePrimaryServerSharedContextRequest(const std::vector<uint8_t> &bytes);

      std::vector<uint8_t> to_bytes() const;

      bytes _encapsulated_secret;
      hpke::HPKE _suite;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::CreatePrimaryServerSharedContextRequest &request);

#endif
