#ifndef PDM_BASE_RESPONSE_H
#define PDM_BASE_RESPONSE_H

#include <pdm/header.h>
#include <pdm/base_request.h>

namespace PDM {
  class BaseResponse {
  public:
    BaseResponse(const PDM::BaseRequest &request);

    BaseResponse(const std::vector<uint8_t> &bytes);

    std::vector<uint8_t> to_bytes() const { return _header.to_bytes(); };

    PDM::Header _header;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::BaseResponse &response);

#endif
