#ifndef PDM_CONSTANTS_H
#define PDM_CONSTANTS_H

#include <stdlib.h>
#include <filesystem>

namespace PDM {
  const char DEFAULT_PRIMARY_SERVER_PORT[] = "3490";
  const char DEFAULT_PRIMARY_CLIENT_PORT[] = "3491";

  const size_t MAXIMUM_BUFFER_SIZE = 100;
  const size_t DEFAULT_PRIMARY_SERVER_BACKLOG = 10;

  const auto DEFAULT_PRIMARY_CLIENT_CONFIG_BASE_DIR = std::filesystem::path(std::string(getenv("HOME"))) / ".pdm_registration" / "primary_client";
  const auto DEFAULT_PRIMARY_SERVER_CONFIG_BASE_DIR = std::filesystem::path(std::string(getenv("HOME"))) / ".pdm_registration" / "primary_server";
}

#endif
