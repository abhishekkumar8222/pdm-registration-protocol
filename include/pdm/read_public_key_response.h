#ifndef PDM_READ_PUBLIC_KEY_RESPONSE_H
#define PDM_READ_PUBLIC_KEY_RESPONSE_H

#include <optional>

#include <pdm/base_response.h>
#include <pdm/read_public_key_request.h>

namespace PDM {
  class ReadPublicKeyResponse : public BaseResponse {
    public:
      ReadPublicKeyResponse(const ReadPublicKeyRequest &request);

      ReadPublicKeyResponse(const bytes &frame);

      bytes to_bytes() const;

      uint16_t _status;
      bytes _serialized_public_key;
      std::optional<PDM::HPKE> _suite;
      std::string _message;
  };
}

std::ostream& operator<<(std::ostream &os, const PDM::ReadPublicKeyResponse &response);

#endif
