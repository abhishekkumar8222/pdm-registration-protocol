pdm_registration_protocol = Proto("PDMReg",  "PDM Registration Protocol")

pdm_version     = ProtoField.int32("pdmreg.pdm_version"     , "PDM Version"    , base.DEC)
packet_length   = ProtoField.int32("pdmreg.length"          , "Length"         , base.DEC)
hpke_version    = ProtoField.int32("pdmreg.hpke_version"    , "HPKE Version"   , base.DEC)
record_type     = ProtoField.int32("pdmreg.record_type"     , "Record Type"    , base.DEC)
seq_num         = ProtoField.int32("pdmreg.seq_num"         , "Sequence Number", base.DEC)
sender_type     = ProtoField.int32("pdmreg.sender_type"     , "Sender Type"    , base.DEC)
authority_level = ProtoField.int32("pdmreg.authority_level" , "Authority Level", base.DEC)
command         = ProtoField.int32("pdmreg.command"         , "Command"        , base.DEC)
req_resp        = ProtoField.int32("pdmreg.req_resp"        , "Req / Resp"     , base.DEC)
transaction_id  = ProtoField.int32("pdmreg.transaction_id"  , "Transaction ID" , base.DEC)
op_code         = ProtoField.int32("pdmreg.op_code"         , "Op Code"        , base.DEC)
reserved        = ProtoField.int32("pdmreg.reserved"        , "Reserved"       , base.DEC)
kem_id          = ProtoField.uint64("pdmreg.kem_id"         , "KEM ID"         , base.DEC)
kdf_id          = ProtoField.uint64("pdmreg.kdf_id"         , "KDF ID"         , base.DEC)
aead_id         = ProtoField.uint64("pdmreg.aead_id"        , "KDF ID"         , base.DEC)
enc_sec         = ProtoField.bytes("pdmreg.enc_sec"         , "Encapsulated Secret", base.NONE)
resp_status     = ProtoField.uint8("pdmreg.resp_status"     , "Status"         , base.DEC)

pdm_registration_protocol.fields = { pdm_version,
                                     packet_length,
                                     hpke_version,
                                     record_type,
                                     seq_num,
                                     sender_type,
                                     authority_level,
                                     command,
                                     req_resp,
                                     transaction_id,
                                     op_code,
                                     reserved,
                                     kem_id,
                                     kdf_id,
                                     aead_id,
                                     enc_sec,
                                     resp_status }

local msg_type_field = Field.new("pdmreg.req_resp")
local function get_msg_type()
  return select(1, msg_type_field())()
end

function pdm_registration_protocol.dissector(buffer, pinfo, tree)
  length = buffer:reported_length_remaining()
  if length == 0 then return end

  pinfo.cols.protocol = pdm_registration_protocol.name

  local subtree = tree:add(pdm_registration_protocol, buffer(), "PDM Registration Protocol")

  subtree:add(pdm_version    , buffer(0,1))
  subtree:add(packet_length  , buffer(1,2))
  subtree:add(hpke_version   , buffer(3,1))
  subtree:add(record_type    , buffer(4,1))
  subtree:add(seq_num        , buffer(5,2))
  subtree:add(sender_type    , buffer(7,1))
  subtree:add(authority_level, buffer(8,1))
  subtree:add(command        , buffer(9,2))
  subtree:add(req_resp       , buffer(11,1))
  subtree:add(transaction_id , buffer(12,2))
  subtree:add(op_code        , buffer(14,1))
  subtree:add(reserved       , buffer(15,1))
  if get_msg_type() == 1 then
    subtree:add(kem_id       , buffer(16,8))
    subtree:add(kdf_id       , buffer(24,8))
    subtree:add(aead_id      , buffer(32,8))
    subtree:add(enc_sec      , buffer(40, length - 40))
  elseif get_msg_type() == 2 then
    subtree:add(resp_status  , buffer(16, 1))
  end

end

local tcp_port = DissectorTable.get("tcp.port")
tcp_port:add(3490, pdm_registration_protocol)
tcp_port:add(3491, pdm_registration_protocol)
